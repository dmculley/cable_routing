cable_routing_07.pdf: cable_routing_07.tex
	pdflatex cable_routing_07.tex
	bibtex cable_routing_07.aux

lastdiff.pdf: cable_routing_07.tex
	git cat-file blob HEAD:cable_routing_07.tex > /tmp/HEAD.tex
	latexdiff --config PICTUREENV=CD /tmp/HEAD.tex cable_routing_07.tex > lastdiff.tex
	pdflatex lastdiff
	bibtex lastdiff
	pdflatex lastdiff
	pdflatex lastdiff
