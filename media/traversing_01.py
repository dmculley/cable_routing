#@author: dmc13


from pylab import *
from math import *
from numpy import *

n = 50

K = 3 + sqrt(1.5**2 + 9) + 4

x = linspace(0, 3, n)
LR_r1 = 3 + sqrt(1.5**2 + 9) + 4 + sqrt(8) + sqrt(x**2 + 1.5**2) + sqrt((4-x)**2 +1)
LR_r2 = 3 + sqrt(1.5**2 + 9) + 4 + sqrt(16 + 2.5**2) + sqrt(x**2 + 1.5**2) + sqrt((x-2)**2 +0.5**2)

LR = zeros(len(x))
switch = []

for i in range(len(x)):
    if LR_r1[i] < LR_r2[i]:
        print x[i]
        LR[i] = LR_r1[i]
    else: 
        switch.append(x[i])
        LR[i] = LR_r2[i]
    

print 'cross-over: ', max(switch)


rc('text', usetex=True)
rc('font', family='serif')

plot(x, LR_r2, label='$l$ for $x \in r_2$')
plot(x, LR_r1, label='$l$ for $x \in r_1$')
plot(x, LR, linewidth=4.0, ls='-.', label='Optimum routing length, $L$')

xlabel(r'\textit{location of turbine,} $x_M$')
ylabel(r'\textit{length of routing,} $l$')


grid(True)
axis('equal')
legend()
title(r'\textit{Routing Lengths for turbine M traversing field}')
#show()
#savefig('traversing_01.png')

from pyx import *

g = graph.graphxy(width=12,
                  x=graph.axis.linear(min=0, max=3, title= r"$x$ position of turbine 11"),y=graph.axis.linear(title= r"cable length of routing"), key=graph.key.key(pos="tl", dist=0.1))
g.plot([graph.data.function("y(x)=3 + sqrt(1.5**2 + 9) + 4 + sqrt(8) + sqrt(x**2 + 1.5**2) + sqrt((4-x)**2 +1)", min=0, max=3, title=r"$R$ for T11 $\in r_2$")], [graph.style.line([style.linestyle.dotted])])
g.plot([graph.data.function("y(x)=3 + sqrt(1.5**2 + 9) + 4 + sqrt(16 + 2.5**2) + sqrt(x**2 + 1.5**2) + sqrt((x-2)**2 +0.5**2)", min=0, max=3, title=r"$R$ for T11 $\in r_3$")], [graph.style.line([style.linestyle.dashed])])
g.plot([graph.data.function("y(x)=3 + sqrt(1.5**2 + 9) + 4 + sqrt(8) + sqrt(x**2 + 1.5**2) + sqrt((4-x)**2 +1)", min=1.77551020409, max=3, title=r"$R_{opt}$"), graph.data.function("y(x)=3 + sqrt(1.5**2 + 9) + 4 + sqrt(16 + 2.5**2) + sqrt(x**2 + 1.5**2) + sqrt((x-2)**2 +0.5**2)", min=0, max=1.77551020409, title=None)], [graph.style.line([style.linestyle.solid, color.rgb.red])])

#g.writeEPSfile("function")
g.writePDFfile("cabling")









